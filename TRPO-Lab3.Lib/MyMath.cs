﻿using System;

namespace TRPO_Lab3.Lib
{
    public class MyMath
    {
        public double Formula(double r, double h)
        {   
            if (r <= 0)
            throw new ArgumentException("Недопустимое значение");
            return   (3.14 * h * h * (r * (1.0 / 3.0) * h));
        }
    }
}
