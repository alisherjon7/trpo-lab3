﻿using System;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;
            int r, h;
            Console.WriteLine("Введите значение:");
            str = Console.ReadLine();
            r = Convert.ToInt32(str);
            Console.WriteLine("Введите значение:");
            str = Console.ReadLine();
            h = Convert.ToInt32(str);
            var result = new MyMath().Formula(r, h); 
            Console.WriteLine("Результат:" + result);
          
        }
    }
}
