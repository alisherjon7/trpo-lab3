﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRPO_Lab3.WebApp.Models
{
    public class AppViewModel
    {
        public double h { set; get; }
        public double r { set; get; }
        public double result { set; get; }

    }
}
