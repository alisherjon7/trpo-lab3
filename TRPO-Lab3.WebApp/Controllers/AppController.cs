﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TRPO_Lab3.Lib;
using TRPO_Lab3.WebApp.Models;

namespace TRPO_Lab3.WebApp.Controllers
{
    public class AppController : Controller
    {
        public IActionResult Index(int h, int r)
        {
            var model = new AppViewModel();
            model.h = h;
            model.r = r;

            if (r <= 0)
                model.r = 1;
            if (h <= 0)
                model.h = 1;

            model.result = new MyMath().Formula(model.r, model.h);
            return View(model); ;
        }

    }
}
