﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPO_Lab3.Wpf
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;


        public MainWindowViewModel()
        { }



        private double _num1;
        private double _num2;
        private double _total;

        public double total
        {
            get
            {
                return _num1 + _num2;
            }
        }
        public double num1
        {
            get
            {
                return _num1;
            }
            set
            {
                _num1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(total)));
            }
        }
        public double num2
        {
            get
            {
                return _num2;
            }
            set
            {
                _num2 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(total)));
            }
        }
    }

}
