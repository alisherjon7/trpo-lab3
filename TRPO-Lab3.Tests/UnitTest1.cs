using NUnit.Framework;
using TRPO_Lab3.Lib;
using System;

namespace TRPO_Lab3.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            const int r = 5;
            const int h = 3;
            const double expected = 141.3;
            var result = new MyMath().Formula(r, h);
            Assert.AreEqual(expected, result, 0.0001);
        }

        [Test]
        public void Test2()
        {
            const int r = -5;
            const int h = 3;
            Assert.Throws<ArgumentException>(() => new MyMath().Formula(r, h));
        }
    }
}